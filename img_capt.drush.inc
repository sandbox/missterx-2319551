<?php

/**
 * @file
 * Show overview of currnet settings for IMC.
 */

/**
 * Implements hook_drush_command().
 */
function img_capt_drush_command() {
  $items = array();
  $items['img-capt-settings'] = array(
    'description' => dt('Show current settings'),
    'arguments' => array(
      'arg1'  => dt('Configuration to show detailed info.'),
      'arg2'  => dt('With arg1, show setting "arg2" in configuration "arg1"'),
      'arg3'  => dt('With arg1 and "arg2", set "arg2" with value "arg3" in configuration "arg1".'),
    ),
    'examples'  => array(
      'Show all configurations'  => 'drush img-capt',
      'Show specific configuration'  => 'drush img-capt "Test case 1"',
      'Set a value' => 'drush img-capt "Test case 1" "weight" "10"',
      'Create configuration' => 'drush img-capt create "Test Case"',
      'Delete configuration' => 'drush img-capt delete "Test Case"',
    ),
    'aliases' => array('img-capt'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function img_capt_drush_help($command) {
  switch ($command) {
    case 'drush:img-capt-settings':
    case 'drush:img-capt':
      $s = "\n" . 'Image Capture & More' . "\n";
      $s .= '====================' . "\n";
      $s .= 'Show current image caption configurations.' . "\n";
      $s .= 'If configuration name is supplied shows its settings.' . "\n";
      $s .= 'Do note: arguments are strings so please surround them' . "\n";
      $s .= 'with a quote "' . "\n\n";
      $s .= 'When a new configuration is created all settings from' . "\n";
      $s .= '"Default" are coppied into the new cofiguration.' . "\n\n";
      $s .= 'If you need to enter a negative value use underscore' . "\n";
      $s .= 'instead of the minus sign: -4 becomes _4' . "\n";
      $s .= 'This is due to drush functionality.' . "\n\n";
      $s .= 'Note' . "\n";
      $s .= '----' . "\n";
      $s .= 'The "Default" configuration can not be deleted.' . "\n";
      return dt($s);
  }
}

/**
 * Implements drush_hook_settings().
 *
 * Callback function for drush img-capt
 */
function drush_img_capt_settings(
  $arg_set_name = NULL,
  $arg_setting_name = NULL,
  $arg_setting_value = NULL) {

  $set_name = $arg_set_name;
  $setting_name = $arg_setting_name;
  $setting_value = $arg_setting_value;

  $settings = img_capt_get_sanetized_settings(TRUE);
  $leave = FALSE;
  $is_config = FALSE;
  $is_command = FALSE;
  $message = '';

  echo "\n";

  // No arguments? Set Default configuration.
  if (is_null($arg_set_name)) {
    $set_name = 'brief';
  }

  // Does the configuration exist?
  if (!is_null($set_name) &&
    (array_key_exists($set_name, $settings) || ($set_name == 'brief'))) {
    $is_config = TRUE;
  }

  // Is it a command?
  if (!is_null($set_name) && in_array($set_name, array('delete', 'create'))) {
    $is_command = TRUE;
  }

  // Leave if neither.
  if (!$is_config && !$is_command) {
    $message = '"' . $set_name;
    $message .= '" is unknown. Exiting.' . "\n";
    $leave = TRUE;
  }

  if ($leave) {
    echo $message;
    return;
  }

  // Missing new configuration name?
  if ($is_command && is_null($setting_name)) {
    echo 'Missing new configuration name, exiting.' . "\n";
    return;
  }

  // Create new configuration.
  if ($is_command && !is_null($setting_name) && ($set_name == 'create')) {
    $settings[$setting_name] = $settings['Default'];
    // Make the rest of the script update.
    $is_command = FALSE;
    $is_config = TRUE;
    $set_name = $setting_name;
    $setting_name = 'weight';
    // Find max weight.
    $max_weight = -1000;
    foreach ($settings as $value) {
      if ($value['weight'] > $max_weight) {
        $max_weight = $value['weight'];
      }
    }
    $setting_value = $max_weight + 1;
  }

  // Delete configuration.
  if ($is_command && !is_null($setting_name) && ($set_name == 'delete')) {
    // Is it the "Default" configuration?
    if ($setting_name == 'Default') {
      echo 'Configuration "Default" can not be deleted.' . "\n";
    }
    else {
      // Is it a valid configuration?
      if (array_key_exists($setting_name, $settings)) {
        unset($settings[$setting_name]);
        variable_set('img_capt_settings', base64_encode(serialize($settings)));
        echo 'Configuration "' . $setting_name . '" deleted.' . "\n";
        // Make the rest of the script update.
      }
      else {
        echo 'Configuration "' . $setting_name . '" does not exist.' . "\n";
      }
    }
    $is_command = FALSE;
    $is_config = TRUE;
    $set_name = 'brief';
    $setting_name = NULL;
  }

  // No arguments passed.
  if ($is_config && ($set_name == 'brief')) {
    // Print the settings name and brief content.
    echo "Saved settings, name => path\n";
    $key_max_len = 0;
    foreach ($settings as $key => $value) {
      if (strlen($key) > $key_max_len) {
        $key_max_len = strlen($key);
      }
    }
    foreach ($settings as $key => $value) {
      echo "\t" . str_repeat(' ', $key_max_len - strlen($key));
      echo $key . " => " . $value['content_path'] . "\n";
    }
    echo "\n";
  }

  // Check if configuration argument was passed in and print it out.
  if ($is_config && ($set_name != 'brief') && is_null($setting_name)) {
    echo "Setting: " . $set_name . "\n";
    // Right alignment of key.
    $key_max_len = 0;
    foreach ($settings[$set_name] as $key => $value) {
      if (strlen($key) > $key_max_len) {
        $key_max_len = strlen($key);
      }
    }
    // Print value.
    foreach ($settings[$set_name] as $key => $value) {
      echo "\t" . str_repeat(' ', $key_max_len - strlen($key));
      echo $key . ' => ' . $value . "\n";
    }
    echo "\n";
  }

  // If value is supplied, store it.
  if ($is_config && !is_null($setting_name) && !is_null($setting_value)) {
    // Does the setting exist?
    if (!array_key_exists($setting_name, $settings[$set_name])) {
      echo 'Setting "' . $setting_name . '" does not exist. Exiting.' . "\n";
      return;
    }
    // Replace _ with - in first position.
    if (substr($setting_value, 0, 1) == '_') {
      $setting_value = '-' . substr($setting_value, 1);
    }
    // Set value.
    $settings[$set_name][$setting_name] = check_plain($setting_value);
    variable_set('img_capt_settings', base64_encode(serialize($settings)));
    // Reset $setting_value so it is printed.
    $setting_value = NULL;
  }

  // Check if setting name and not setting value is passed.
  if ($is_config && !is_null($setting_name) && is_null($setting_value)) {
    echo $set_name . "\n\t" . $setting_name . ' => ';
    echo $settings[$set_name][$setting_name] . "\n";
  }
}
